*** Settings ***
Documentation   Recursos disponiveis para interação com a SuperHeroAPI
Library         RequestsLibrary
Library         Collections

*** Variables ***
${TOKEN}        1999613633481713
${URL_API}      https://superheroapi.com/api/${TOKEN}

*** Keywords ***
Conectar API
    Create Session      SuperHeroAPI     ${URL_API}   verify=True

Validar conexão com API
    ${CONN}     Get Request    SuperHeroAPI     /
    Should Be Equal As Strings  ${CONN.status_code}   200

Dado que esteja conectado na SuperHeroAPI
    Run Keyword    Validar conexão com API

Buscar historico e ID do super herói "${HERO_NAME}"
    ${HISTORIC}   Get Request     SuperHeroAPI     /search/${HERO_NAME}
    ${HEROES}     Set Variable    ${HISTORIC.json()['results']}

    :FOR  ${HERO}  IN  @{HEROES}
    \  ${HERO_ID}=          Set Variable If    '${HERO['name']}' == '${HERO_NAME}'    ${HERO['id']}
    \  Run Keyword If      '${HERO['name']}' == '${HERO_NAME}'    Exit For Loop

    Should Not Be Empty    ${HERO_ID}
    Set Test Variable      ${HERO_ID}
    Log    ${HERO_ID}

Quando requisitar o histórico do super herói "${HERO_NAME}"
    ${NAME}      Set Variable    "${HERO_NAME}"
    Run Keyword  Buscar historico e ID do super herói ${NAME}

Então deve ser retornado que a sua inteligência é superior a "${IQ}"
    ${POWER_STATS}    Get Request         SuperHeroAPI     /${HERO_ID}/powerstats
    ${EXPECTED_IQ}    Set Variable        ${POWER_STATS.json()['intelligence']}
    Should Be True    ${EXPECTED_IQ} > ${IQ}
    Log               Seu QI: ${EXPECTED_IQ}

E deve ser retornado que o seu verdadeiro nome é ser "${REAL_NAME}"
    ${BIOGRAPHY}           Get Request    SuperHeroAPI     /${HERO_ID}/biography
    ${EXPECTED_REAL_NAME}  Set Variable   ${BIOGRAPHY.json()['full-name']}
    Should Be Equal As Strings    ${EXPECTED_REAL_NAME}    ${REAL_NAME}
    Log                    Seu nome real: ${REAL_NAME}

E deve ser retornado que é afiliado do grupo "${GROUP}"
    ${CONNECTIONS}         Get Request     SuperHeroAPI    /${HERO_ID}/connections
    ${GROUP_LIST}          Set Variable    ${CONNECTIONS.json()['group-affiliation']}
    Should Contain         ${GROUP_LIST}   ${GROUP}
    Log                    Grupo Afiliado: ${GROUP}

#---------------------------------------------------------------------------------------------

Quando requisitar as estatísticas de poder dos super heróis "${HERO_A}" e "${HERO_B}"
    Buscar historico e ID do super herói "${HERO_A}"
    ${POWER_STATS}         Get Request         SuperHeroAPI     /${HERO_ID}/powerstats
    ${STATS_OF_HERO_A}     Set Variable        ${POWER_STATS.json()}
    Should Not Be Empty    ${STATS_OF_HERO_A}
    Set Test Variable      ${STATS_OF_HERO_A}

    Buscar historico e ID do super herói "${HERO_B}"
    ${POWER_STATS}         Get Request         SuperHeroAPI     /${HERO_ID}/powerstats
    ${STATS_OF_HERO_B}     Set Variable        ${POWER_STATS.json()}
    Should Not Be Empty    ${STATS_OF_HERO_B}
    Set Test Variable      ${STATS_OF_HERO_B}
    Log    Estatísticas de poder para ${HERO_A} e ${HERO_B}: OK !

Então deve ser retornado que o mais inteligente é o herói "${HERO_B}"
    Should Be True   ${STATS_OF_HERO_B['intelligence']} > ${STATS_OF_HERO_A['intelligence']}
    Log     ${STATS_OF_HERO_B['name']} tem o QI ${STATS_OF_HERO_B['intelligence']}, Maior que o ${STATS_OF_HERO_A['name']}

E deve ser retornado que o mais rápido é o herói "${HERO_A}"
    Should Be True   ${STATS_OF_HERO_A['speed']} > ${STATS_OF_HERO_B['speed']}
    Log     ${STATS_OF_HERO_A['name']} é o mais rapido: ${STATS_OF_HERO_A['speed']}

E deve ser retornado que o mais forte é o herói "${HERO_B}"
    Should Be True   ${STATS_OF_HERO_B['strength']} > ${STATS_OF_HERO_A['strength']}
    Log     ${STATS_OF_HERO_B['name']} é o mais forte: ${STATS_OF_HERO_B['strength']}
